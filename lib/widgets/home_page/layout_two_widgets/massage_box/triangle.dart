//packages
import 'package:flutter/material.dart';
import 'dart:math';

class TriangleBuilder extends StatelessWidget {
  final Color color;
  final bool _isActive;

  TriangleBuilder(this.color, this._isActive);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 10.0,
      width: 20.0,
      child: ClipRect(
        child: OverflowBox(
          maxWidth: 20.0,
          maxHeight: 20.0,
          child: Align(
            alignment: Alignment.topCenter,
            child: Transform.translate(
              offset: Offset(.0, -11.0),
              child: Transform.rotate(
                angle: pi / 4,
                child: Container(
                  width: 20.0,
                  height: 20.0,
                  decoration: BoxDecoration(
                    color: color,
                    borderRadius: BorderRadius.all(
                      Radius.circular(3.0),
                    ),
                    boxShadow: <BoxShadow>[
                      !_isActive
                          ? BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.12),
                              offset: Offset(3, 3.0),
                              blurRadius: 4.0,
                            )
                          : BoxShadow(
                              color: Color.fromRGBO(225, 255, 255, 0.0),
                              offset: Offset(0, 0.0),
                              blurRadius: 0.0,
                            ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
