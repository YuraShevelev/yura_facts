//packages
import 'package:flutter/material.dart';

//widgets
import 'package:facts_app/widgets/home_page/layout_two_widgets/massage_box/triangle.dart';
import 'package:flutter/widgets.dart';
import '../../../../handlers/button.dart';

class MessageBoxBuilder extends StatefulWidget {
  final bool active;
  final String text;
  final String secondText;
  final int buttonNumber;

  MessageBoxBuilder(
      {@required this.active, this.text, this.secondText, this.buttonNumber});
  @override
  State<StatefulWidget> createState() {
    return _MessageBoxBuilderState();
  }
}

class _MessageBoxBuilderState extends State<MessageBoxBuilder> {
  bool _isActive;
  double height;
  double width;
  Color textColor;
  Color secondTextColor;
  Color itemColor;
  BoxDecoration decoration;


  @override
  Widget build(BuildContext context) {
    _isActive = widget.active;
    if (_isActive) {
      height = 35;
      width = 64;
      textColor = Colors.white;
      secondTextColor = Color.fromRGBO(0, 0, 0, 0.54);
      itemColor = Theme.of(context).dialogBackgroundColor;
      decoration = BoxDecoration(
        color: itemColor,
        borderRadius: BorderRadius.all(
          Radius.circular(3.0),
        ),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.12),
            offset: Offset(0, 0),
            blurRadius: 2.0,
          ),
        ],
      );
    } else {
      height = 30;
      width = 57;
      textColor = Color.fromRGBO(105, 113, 171, 1);
      secondTextColor = Color.fromRGBO(0, 0, 0, 0.54);
      itemColor = Colors.white;
      decoration = BoxDecoration(
        color: itemColor,
        borderRadius: BorderRadius.all(
          Radius.circular(3.0),
        ),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.24),
            offset: Offset(0, 4.0),
            blurRadius: 5.0,
          ),
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.12),
            offset: Offset(0, 0),
            blurRadius: 2.0,
          ),
        ],
      );
    }
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            height: height,
            width: width,
            margin: EdgeInsets.only(
              left: 17.5,
              right: 17.5,
              top: 16,
            ),
            child: Text(
              widget.text,
              style: TextStyle(
                color: textColor,
                fontSize: 17,
                fontFamily: 'Rubik-Medium',
              ),
              ),
            decoration: decoration,
          ),
          TriangleBuilder(itemColor, _isActive),
          Container(
            child: Text(
              widget.secondText,
              style: TextStyle(
                color: secondTextColor,
                fontSize: 17,
                fontFamily: 'Rubik-Medium',
              ),
            ),
          ),
        ],
      ),
    );
  }
}
