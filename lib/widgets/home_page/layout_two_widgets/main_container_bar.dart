//packages
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

//handlers
import '../../../handlers/button.dart';

//widgets
import './massage_box/message_box.dart';

class MainContainerBar extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MainContainerBarState();
  }
}

class _MainContainerBarState extends State<MainContainerBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          InkWell(
            child: MessageBoxBuilder(
              active: buttonHandler.firstButton,
              text: '48',
              secondText: 'לא אוהב',
            ),
            onTap: () {
              setState(() {
                buttonHandler.onFirstButton();
              });
            },
          ),
          InkWell(
            child: MessageBoxBuilder(
              active: buttonHandler.secondButton,
              text: '890',
              secondText: 'אני יודע',
            ),
            onTap: () {
              setState(() {
                buttonHandler.onSecondButton();
              });
            },
          ),
          InkWell(
            child: MessageBoxBuilder(
              active: buttonHandler.thirdButton,
              text: '1785',
              secondText: 'כמו',
            ),
            onTap: () {
              print('text');
              setState(() {
                buttonHandler.onThirdButton();
              });
            },
          ),
        ],
      ),
    );
  }
}
