//packages
import 'package:flutter/material.dart';

class MainContainerTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 16, bottom: 10, left: 16,right: 28),
      child: Text(
        'הידעת ש…',
        style: TextStyle(
            color: Theme.of(context).textSelectionHandleColor,
            fontSize: 35,
            fontFamily: 'Assistant-Bold',
            fontWeight: FontWeight.bold),
      ),
    );
  }
}
