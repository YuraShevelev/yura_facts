//packages
import 'package:flutter/material.dart';


//widgets
import './main_container_title.dart';
import './main_container_text.dart';
import './main_container_bar.dart';

class MainContainerBuild extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
  final double deviceHeight = MediaQuery.of(context).size.height;
  final double diviceWidth = MediaQuery.of(context).size.width;
  final double boxHeight = deviceHeight / 1.55;
    return Column(
      children: <Widget>[
        Container(
          height: boxHeight / 6,
        ),
        Container(
          decoration: BoxDecoration(
            color: Colors.white.withOpacity(1),
            border: Border.all(width: 1, color: Color.fromRGBO(127, 134, 195, 0.1)),
            borderRadius: BorderRadius.all(
              Radius.circular(8.0),
            ),
          ),
          height: boxHeight / 1.1,
          width: diviceWidth,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              MainContainerTitle(),
              MainContainerText(),
              MainContainerBar(),
            ],
          ),
        ),
      ],
    );
  }
}
