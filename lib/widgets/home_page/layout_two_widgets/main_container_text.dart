//packages
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';


class MainContainerText extends StatelessWidget {

  Widget build(BuildContext context) {
    final double deviceHeight = MediaQuery.of(context).size.height;
    final double textBoxHeight = deviceHeight / 3;

    return Container(
      decoration: BoxDecoration(
        border: Border(right: BorderSide(color: Colors.grey.withOpacity(0.7)))
      ),
      margin: EdgeInsets.only(
        left: 38,
        right: 14,
      ),
      height: textBoxHeight,
      child: listViewBuilder(),
    );
  }
}

Widget listViewBuilder() {
  String text =
      'מונח ה"צבר" הישראלי נטבע על ידי אורי קיסרי ב-18 באפריל 1931 במאמר "אנחנו עלי-הצבר!" ובו טען נגד קיפוח ילידי הארץ כנגד המהגרים החדשים.';
  String text2 =
      'הפופולריות של מונח זה בתרבות הישראלית התגלגלה גם לבקבוק ליקר הסברה הישראלי, אשר עוצב בדמות האמפורה היוונית, ברוח תנועת "הכנענים".מונח ה"צבר" הישראלי נטבע על ידי אורי קיסרי ב-18 באפריל 1931 במאמר "אנחנו עלי-הצבר!" ובו טען נגד קיפוח ילידי הארץ כנגד המהגרים החדשים.';
  String text3 =
      'הפופולריות של מונח זה בתרבות הישראלית התגלגלה גם לבקבוק ליקר הסברה הישראלי, אשר עוצב בדמות האמפורה היוונית, ברוח תנועת "הכנענים"';
  return Directionality(
    textDirection: TextDirection.ltr,
    child: Scrollbar(
      child: Directionality(
        textDirection: TextDirection.rtl,
        child: ListView(
          padding: EdgeInsets.only(right: 14),
          children: <Widget>[
            Text(
              text,
              style: TextStyle(
                color: Color.fromRGBO(0, 0, 0, 0.54),
                fontFamily: 'Assistant-Regular',
                fontWeight: FontWeight.w400,
              ),
            ),
            Text(''),
            Text(text2,
                style: TextStyle(
                  color: Color.fromRGBO(0, 0, 0, 0.54),
                  fontFamily: 'Assistant-Regular',
                  fontWeight: FontWeight.w400,
                )),
            Text(''),
            Text(text2,
                style: TextStyle(
                  color: Color.fromRGBO(0, 0, 0, 0.54),
                  fontFamily: 'Assistant-Regular',
                  fontWeight: FontWeight.w400,
                )),
            Text(''),
            Text(text2,
                style: TextStyle(
                  color: Color.fromRGBO(0, 0, 0, 0.54),
                  fontFamily: 'Assistant-Regular',
                  fontWeight: FontWeight.w400,
                )),
            Text(''),
            Text(text3,
                style: TextStyle(
                  color: Color.fromRGBO(0, 0, 0, 0.54),
                  fontFamily: 'Assistant-Regular',
                  fontWeight: FontWeight.w400,
                )),
          ],
        ),
      ),
    ),
  );
}
