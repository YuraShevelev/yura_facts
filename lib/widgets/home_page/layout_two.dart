//packages
import 'package:flutter/material.dart';

//widgets
import './layout_two_widgets/character.dart';
import './layout_two_widgets/main_container.dart';

class LayoutTwo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final double deviceHeight = MediaQuery.of(context).size.height;
    final double boxHeight = deviceHeight / 1.4;
    return Container(
      decoration: BoxDecoration(
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Color.fromRGBO(31, 31, 31, 0.1),
            offset: Offset(0, 24.0),
            blurRadius: 24.0,
          ),
          BoxShadow(
            color: Color.fromRGBO(59, 59, 59, 0.1),
            offset: Offset(0, 0),
            blurRadius: 24.0,
          ),
        ],
      ),
      height: boxHeight,
      margin: EdgeInsets.only(
          left: 15, right: 15, bottom: 15, top: deviceHeight / 7),
      child: Stack(
        alignment: Alignment.topLeft,
        children: <Widget>[
          MainContainerBuild(),
          CharacterBuild(),
        ],
      ),
    );
  }
}
