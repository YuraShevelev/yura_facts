//packages
import 'package:flutter/material.dart';

//widgets
import './top_bar_title.dart';
import './top_bar_menu.dart';

class TopBarBuilder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;
    return Container(
      height: 185,
      width: deviceWidth,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: new ExactAssetImage('assets/background.jpg'),
          fit: BoxFit.cover,
        ),
      ),
      child: Container(
        color: Color.fromRGBO(47, 47, 47, 0.5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            AppBar(
              actions: <Widget>[
                Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(left: 16),
                  child: Text(
                    'יום חמישי, 12 בפברואר2019',
                    style: TextStyle(
                      fontSize: 17,
                      fontFamily: 'Lucida-Grande',
                    ),
                  ),
                ),
              ],
              centerTitle: true,
              elevation: 0,
              backgroundColor: Colors.transparent,
            ),
            Expanded(
              flex: 2,
              child: Container(
                alignment: Alignment.bottomRight,
                child: TopBarMenuBuilder(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
