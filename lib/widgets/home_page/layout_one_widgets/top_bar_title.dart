//packages
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';



class TopBarTitleBuilder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Container(
              alignment: Alignment.centerRight,
              margin: EdgeInsets.only(
                top: 36,
                right: 16,
              ),
              child: IconButton(
                icon: Icon(
                  Icons.menu,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.only(top: 42, left: 16),
              child: Text(
                'יום חמישי, 12 בפברואר 2019',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                    fontFamily: 'Lucida-Grande'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}



