//packages
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class BottomButton extends StatelessWidget {
  final String titleText;
  final Color buttonColor;
  final Function onPressed;

  BottomButton({this.titleText, this.buttonColor, this.onPressed});

  @override
  Widget build(BuildContext context) {
    final double diviceeHeight = MediaQuery.of(context).size.height;
    final double diviceeWidth = MediaQuery.of(context).size.width;
    final double buttonHeight = diviceeHeight / 20;
    final double buttonwidth = diviceeWidth / 3.5;
    return InkWell(
      onTap: (){
        print('Hello2');
      },
      child: Center(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 2.5),
          height: 32,
          width: 106,
          decoration: BoxDecoration(
            color: buttonColor,
            borderRadius: BorderRadius.all(
              Radius.circular(15.0),
            ),
          ),
          child: Text(
            titleText,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 21,
              color: Colors.white,
              fontFamily: 'Rubik-Medium',
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
      ),
    );
  }
}
