//packages
import 'package:flutter/material.dart';

//widgets
import './bottom_buttons.dart';

class BottomBarBuilder extends StatelessWidget {
  List<String> tagText = [
    'תיירות',
    'אריה',
    'חיות',
    'גן חיות',
  ];
  @override
  Widget build(BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;
    return Container(
      //padding: EdgeInsets.symmetric(horizontal: 10),
      color: Theme.of(context).primaryColor,
      height: 85,
      width: deviceWidth,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: 4,
        itemBuilder: (BuildContext context, int index) {
          return BottomButton(
            titleText: tagText[index],
            buttonColor: index == 1
                ? Theme.of(context).dialogBackgroundColor
                : Theme.of(context).buttonColor,
          );
        },
      ),
    );
  }
}
