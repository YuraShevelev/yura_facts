//packages
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class TopBarMenuBuilder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final double menuHeight = 41;
    final double menuWidth = 157;
    return Container(
      height: menuHeight,
      width: menuWidth,
      margin: EdgeInsets.symmetric(horizontal: 16, vertical: 32),
      decoration: BoxDecoration(
        color: Theme.of(context).accentColor,
        borderRadius: BorderRadius.all(
          Radius.circular(23.0),
        ),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.2),
            offset: Offset(0, 15.0),
            blurRadius: 24.0,
          ),
        ],
      ),
      child: Row(
        children: <Widget>[
          InkWell(
            onTap: () {
              print('share');
            },
            child: Container(
              margin: EdgeInsets.only(right: 20),
              child: SvgPicture.asset(
                'assets/share.svg',
                fit: BoxFit.cover,
                color: Colors.white,
                height: 28,
                width: 26,
              ),
            ),
          ),
          InkWell(
            onTap: () {
              print('facebook');
            },
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 25),
              child: SvgPicture.asset(
                'assets/facebook.svg',
                fit: BoxFit.cover,
                color: Colors.white,
                height: 23,
                width: 13,
              ),
            ),
          ),
          InkWell(
            onTap: () {
              print('twitter');
            },
            child: Container(
              child: SvgPicture.asset(
                'assets/twitter.svg',
                color: Colors.white,
                height: 23,
                width: 29,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
