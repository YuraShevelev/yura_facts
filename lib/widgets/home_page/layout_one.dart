//packages;
import 'package:flutter/material.dart';

//widgets
import './layout_one_widgets/bottom_bar.dart';
import './layout_one_widgets/top_bar.dart';

class LayoutOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double deviceHeight = MediaQuery.of(context).size.height;
    final double lineHeight = deviceHeight /2;
    return Column(
      children: <Widget>[
        TopBarBuilder(),
        Expanded(
          flex: 4,
          child: Container(
            width: deviceWidth,
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    margin: EdgeInsets.only(top: 14, bottom: 88),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(5.0),
                          bottomLeft: Radius.circular(5.0),
                        ),
                        color: Color.fromRGBO(0, 0, 0, 0.2)),
                  ),
                ),
                Expanded(
                  flex: 55,
                  child: Container(),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    margin: EdgeInsets.only(top: 14, bottom: 88),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(5.0),
                          bottomRight: Radius.circular(5.0),
                        ),
                        color: Color.fromRGBO(0, 0, 0, 0.2)),
                  ),
                ),
              ],
            ),
          ),
        ),
        BottomBarBuilder(),
      ],
    );
  }
}
