//packages
import 'package:flutter/material.dart';

//handlers
import './settings.dart';

class ColorHandler {
  Map themes = {
    'Purple': {
      //Text
      settingsHandler.textSelectionColor:
          const Color.fromRGBO(255, 255, 255, 1),
      settingsHandler.textSelectionHandleColor:
          const Color.fromRGBO(105, 113, 171, 1),

      //Fone
      settingsHandler.primaryColor:
          const Color.fromRGBO(127, 134, 195, 1),
      settingsHandler.accentColor: 
          const Color.fromRGBO(127, 134, 195, 0.5),

      //Buttons
      settingsHandler.dialogBackgroundColor:
          const Color.fromRGBO(254, 201, 59, 1),
      settingsHandler.buttonColor: 
          const Color.fromRGBO(226, 226, 226, 0.4),
    },
  };
}

ColorHandler colorHandler = new ColorHandler();
