//package
import 'package:flutter/material.dart';


class ButtonHandler{
  bool firstButton = false;
  bool secondButton = false;
  bool thirdButton = false;

  void onFirstButton(){
    if(firstButton == true){
      firstButton = false;
      return;
    }
    firstButton = true;
    secondButton = false;
    thirdButton = false;
  }
  void onSecondButton(){
    if(secondButton == true){
      secondButton = false;
      return;
    }
    firstButton = false;
    secondButton = true;
    thirdButton = false;
  }
  void onThirdButton(){
    if(thirdButton == true){
      thirdButton = false;
      return;
    }
    firstButton = false;
    secondButton = false;
    thirdButton = true;
  }

}
ButtonHandler buttonHandler = ButtonHandler();