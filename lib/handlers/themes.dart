//packages
import 'package:flutter/material.dart';

//handlers
import './settings.dart';
import '../handlers/colors.dart';

class Themes {

//purpleTheme 
ThemeData purpleTheme = new ThemeData(

    //Text
    textSelectionColor: colorHandler.themes['Purple']
        [settingsHandler.textSelectionColor],
    textSelectionHandleColor: colorHandler.themes['Purple']
        [settingsHandler.textSelectionHandleColor],

    //Buttons
    dialogBackgroundColor: colorHandler.themes['Purple']
        [settingsHandler.dialogBackgroundColor],
    buttonColor: colorHandler.themes['Purple']
        [settingsHandler.buttonColor],

    //Fons
    primaryColor: colorHandler.themes['Purple']
        [settingsHandler.primaryColor],
    accentColor: colorHandler.themes['Purple']
        [settingsHandler.accentColor],
  );
}

Themes themes = new Themes();
