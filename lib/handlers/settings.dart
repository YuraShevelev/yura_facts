class SettingsHandler{
  //Theme colors
  //Theme Text
  String textSelectionColor = 'textSelectionColor';
  String textSelectionHandleColor = 'textSelectionHandleColor';
  
  //Theme buttons
  String dialogBackgroundColor = 'dialogBackgroundColor';
  String buttonColor = 'buttonColor';

  //Theme fone
  String primaryColor = 'primaryColor';
  String accentColor = 'accentColor';


}
SettingsHandler settingsHandler = new SettingsHandler();