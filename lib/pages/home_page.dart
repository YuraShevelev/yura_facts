//packages
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

//widgets
import '../widgets/home_page/layout_one.dart';
import '../widgets/home_page/layout_two.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        drawer: Drawer(
        ),
        body: Stack(
          children: <Widget>[      
            LayoutOne(),     
            LayoutTwo(),      
          ],
        ),
      ),
    );
  }
}
